/**
 * 建立容器元件
 * @see ui/Container.js
 */
var win = require("ui/Container").getWindowView(
{
    //設定容器視圖初始參數
    navBarHidden:true,
    modal:true,
    backgroundColor:"#eee"   
});
/**
 * 建立label元件
 * @see ui/LabelView.js
 */
var label = require("ui/LabelView").getLabelView(
{
    //設定初始參數
    // text:"hellow world",
    text:L("text_label"),
    font:
    {
        fontSize:"18sp"
    },
    color:"#000"        
}
);
label.bindEvent();
win.add(label);
win.open();
