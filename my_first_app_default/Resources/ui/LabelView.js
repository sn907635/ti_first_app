/**
 * @author Nick.H
 */
var label;
function getLabel(params)
{
    labelView = Ti.UI.createLabel(params);
    labelView.id = "test";    
    label = labelView;
    //定義label元件的bindEvent方法
    label.bindEvent = bindEvent;
    return label;
}
function bindEvent()
{
    label.addEventListener("click",function(e)
    {
        alert(e.source.text);
    });
}
exports.getLabelView = getLabel;
