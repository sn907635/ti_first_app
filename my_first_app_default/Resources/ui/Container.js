/**
 * @author Nick.H
 */
var win;
function getWindow(params)
{
    win = Ti.UI.createWindow(params);
    return win;
}
exports.getWindowView = getWindow;
